<?php
session_start();
if (isset($_GET['page'])) {
  $page = $_GET['page'];
} else {
  $page = "main";
}


?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>smh</title>
  <!-- Font Awesome Icons
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="css/all.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte_blue.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="css/ionicons.min.css">
  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- This is what you need -->
    <script src="alert/dist/sweetalert.js"></script>
    <link rel="stylesheet" href="alert/dist/sweetalert.css">
    <!--.......................-->
</head>
<body class="hold-transition sidebar-collapse">
<script>
$(document).ready(function(){

  var jwt_token = localStorage.getItem("jwt_token");

  if (jwt_token != "" && jwt_token != null) {
    var currentTime = getCurrentTime();

    var expireTime = getExpireDate(jwt_token);

    //alert(expireTime);

    if (currentTime > expireTime) {
      $('#login_form').show();
      $('#user_form').hide();
    } else {
      var user = localStorage.getItem("user");
      if (user != "" && user != null) {
        user = JSON.parse(user);
        //alert(user.name);
        $('#full_name').html(user.name+" "+user.last_name);
        $('#full_name1').html(user.name+" "+user.last_name);
        $('#position').html(user.job_position);
        $('#ogan').html(user.code);
        $('#jumbo').hide();
        $("#username").val('');
        $("#password").val('');
        $('#login_form').hide();
      }
    }

  } else {
    $('#login_form').show();
    $('#user_form').hide();
  }

});

function getCurrentTime() {
  var time = Math.floor(Date.now()/1000);
  return time;
}

function getExpireDate(jwt_token) {
  var expire = 0;

  if (jwt_token != "" && jwt_token != null) {
    expire = JSON.parse(atob(jwt_token.split(".",)[1]));
    expire = expire.exp;
    return expire;
  }

}

function confirm_out() {
        //alert("sss");
        swal({
        title: "ต้องการออกจากระบบใช่ใหม?",
        text: "ควรออกจากระบบทุกครั้ง เมื่อเลิกใช้งาน",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "ออกจากระบบ",
        closeOnConfirm: false
        },
        function(){
          localStorage.clear("jwt_token");
          localStorage.clear("user");
          window.location = '../../smh1/?page=main';
        });
}
function datethai(bdate) {
  if(bdate != '' && bdate != null) {
    var thmonth = new Array ("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    var y = bdate.substr(0, 4);
    var m = parseInt(bdate.substr(4, 2));
    var d = bdate.substr(6, 2);
    var str_m = thmonth[m];
    var edate = d+" "+str_m+" "+y;
  } else {
    edate = "";
  }
  return edate;
}

function datethai1(bdate) {
  if(bdate != '' && bdate != null) {
    var thmonth = new Array ("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    var y = parseInt(bdate.substr(0, 4));
    var m = parseInt(bdate.substr(5, 2));
    var d = bdate.substr(8, 2);
    var str_m = thmonth[m];
    y = y + 543;
    var edate = d+" "+str_m+" "+y;
  } else {
    edate = "";
  }
  return edate;
}

</script>
<div class="wrapper">

  <!-- Navbar -->
  <?php include './includes/header.php'; ?>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <?php include './includes/sidebar.php'; ?>
  <?php include $page.'.php'; ?>

  <!-- Main Footer -->
  <?php  include './includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

</body>
</html>
