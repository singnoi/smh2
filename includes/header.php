  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-primary navbar-dark border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="index.php"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../../smh1/index.php" class="nav-link"> <i class="fa fa-home"> </i>  หน้าแรก</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="http://203.157.144.198/web/" class="nav-link"> <i class="fa fa-globe"> </i>  หน้าเว็บ รพ.</a>
      </li>



    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown" id="user_form">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-user-circle"></i>
            <span id="full_name"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right dropdown-menu-blue">
          <a href="#" class="dropdown-item dropdown-item-blue">
            <!-- Message Start -->
            <div class="media">
              <?php

              ?>
              <img src="../../smh1/dist/img/avatar5.png" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  <i class="fa fa-angle-right mr-1"></i> ชื่อ <span id="full_name1"></span>
                </h3>
                <p class="text-sm"><i class="fa fa-angle-right mr-1"></i> ตำแหน่ง <span id="position"></span></p>
                <p class="text-sm text-white"><i class="fa fa-angle-right mr-1"></i> รหัส รพ. <span id="ogan"></span></p>
              </div>
            </div>
            <!-- Message End -->
          </a>

          <div class="dropdown-divider"></div>

          <div class="dropdown-divider"></div>
          <a href="#" onclick="confirm_out();" class="dropdown-item dropdown-footer bg-warning"> <i class="fa fa-sign-out" aria-hidden="true"></i> ออกจากระบบ</a>
        </div>
      </li>


<link rel="stylesheet" href="../../smh1/login/login.css">


                    <li class="nav-item dropdown" id="login_form">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-sign-in-alt" aria-hidden="true"></i>
                        ลงชื่อเข้าใช้ <span class="caret"></span>
                      </a>
            			<ul id="login-dp" class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
            				<li>
            					 <div class="row">
            							<div class="col-md-12">

                            <div class="social-buttons">
                              LOGIN ด้วยรหัสเข้าใช้อินเตอร์เน็ต
                            </div>

            								 <form class="form" role="form" method="post" accept-charset="UTF-8" id="login_bar">
                               <input type="hidden" name="url" id="url" value="smh1">
            										<div class="form-group">
            											 <label class="sr-only" for="username">username</label>
            											 <input type="text" class="form-control" id="username" placeholder="ชื่อผู้ใช้" autocomplete="off" required>
            										</div>
            										<div class="form-group">
            											 <label class="sr-only" for="password">Password</label>
            											 <input type="password" class="form-control" id="password" placeholder="รหัสผ่าน" required>
                                                         <div class="help-block text-right"><a href="">ลืมรหัสผ่าน ?</a></div>
            										</div>
            										<div class="form-group">
            											 <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt" aria-hidden="true"></i> เข้าสู่ระบบ</button>
            										</div>
                                <div id="check_login">
                                </div>

            								 </form>
            							</div>

            					 </div>
            				</li>
            			</ul>
                    </li>

                    <script>
                    $('#login_bar').submit(function(e){
                      e.preventDefault();
                      var username = $("#username").val();
                      var password = $("#password").val();
                      var url = $("#url").val();

                      var settings = {
                          "async": true,
                          "crossDomain": true,
                          "url": "https://smarthealth.service.moph.go.th/phps/public/api/v3/gettoken",
                          "method": "POST",
                          "headers": {
                            "Content-Type": "application/json"
                          },
                          "processData": false,
                          "data": JSON.stringify({"username":username,"password":password})
                        }

                        $.ajax(settings).done(function (response) {
                          console.log(response);
                          if (response.jwt_token != "" && response.jwt_token != null) {
                            localStorage.setItem("jwt_token",response.jwt_token);
                            localStorage.setItem("user",JSON.stringify(response.user));
                            var user = localStorage.getItem("user");
                            user = JSON.parse(user);

                            swal({
                                    title: "ลงชื่อเข้าใช้ระบบสำเร็จ",
                                    text: "โดย "+user.name+" "+user.last_name,
                                    closeOnClickOutside: false,
                                    type: "success"

                                }, function () {
                                  $("#username").val('');
                                  $("#password").val('');
                                  $('#login_form').hide();
                                  $('#user_form').show();
                                  $('#full_name').html(user.name+" "+user.last_name);
                                  $('#full_name1').html(user.name+" "+user.last_name);
                                  $('#position').html(user.job_position);
                                  $('#ogan').html(user.code);
                                  $('#jumbo').hide();

                                });
                          } else {
                            swal({
                                title: "ลงชื่อเข้าใช้ ไม่สำเร็จ",
                                text: response.message,
                                closeOnClickOutside: false,
                                type: "warning"
                                }, function () {

                              });
                          }

                        });


                    });

                    </script>

    </ul>
  </nav>
  <!-- /.navbar -->
