<?php
function member_layer($num)
{
    switch ($num) {
        case '1':
            $mtype = "หัวหน้างานยานพาหนะ";
            break;

        case '2':
            $mtype = "ผู้บริหาร";
            break;

        case '3':
            $mtype = "แพทย์พยาบาล";
            break;
        case '4':
            $mtype = "บุคลากร";
            break;

        case '5':
            $mtype = "พนักงานขับรถ";
            break;
        default:
            # code...
            break;
    }
    return $mtype;
}
function check_admin($num)
{
    switch ($num) {
        case 'Y':
            $admin = "admin";
            break;
        case 'N':
            $admin = "";
            break;
        default:
            $admin = "";
            break;
    }
    return $admin;
}
function bg_car($id)
{
    switch ($id) {
        case '1':
            $bg = "bg-info";
            break;
        case '4':
            $bg = "bg-primary";
            break;
        case '5':
            $bg = "bg-warning";
            break;
        case '6':
            $bg = "bg-gray";
            break;
        default:
            $bg = "bg-danger";
            break;
    }
    return $bg;
}
function fa_car($id)
{
    switch ($id) {
        case '1':
            $fa = "fa-ambulance";
            break;
        case '4':
            $fa = "fa-bus";
            break;
        case '5':
            $fa = "fa-car";
            break;
        case '6':
            $fa = "fa-truck";
            break;
        default:
            $fa = "fa-taxi";
            break;
    }
    return $fa;
}

function car_status_icon($id)
{
    switch ($id) {
        case '1':
            $icon_status = "<i class=\"fas fa-car text-success\"></i>";
            break;
        case '2':
            $icon_status = "<i class=\"fas fa-car-crash text-warning\"></i>";
            break;
        case '3':
            $icon_status = "<i class=\"fas fa-times text-danger\"></i>";
            break;
        case '4':
            $icon_status = "<i class=\"fas fa-car-side text-info\"></i>";
            break;
        default:
            $icon_status = "<i class=\"fas fa-car text-success\"></i>";
            break;
    }
    return $icon_status;
}

function icon_y($Y)
{
    switch ($Y) {
        case 'Y':
            $icon_check = "<i class=\"fa fa-check-circle fa-1x text-success\"> </i>";
            break;
        case 'N':
            $icon_check = "<i class=\"fa fa-times fa-2x text-danger\"></i>";
            break;
        default:
            $icon_check = "<i class=\"fa fa-circle-o fa-1x\"> </i>";
            break;
    }
    return $icon_check;
}

function year_eng_to_date($strDate){
    $arr = array();
    $arr = explode('/', $strDate);
    $strYear=$arr[2];
    $newDate = $strYear.'-'.$arr[1].'-'.$arr[0];
    return "$newDate";
}
function date_to_input($strDate){
    $strYear = date("Y",strtotime($strDate));
    $strMonth= date("m",strtotime($strDate));
    $strDay= date("d",strtotime($strDate));
    return "$strDay/$strMonth/$strYear";
}
?>
