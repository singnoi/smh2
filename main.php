  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <link href="../../smh1/alert/assets/docs.css" rel="stylesheet">

    <div class="jumbotron" id="jumbo">
      <h1>ระบบตรวจสอบ Smart Health ID </h1>

  <div class="row justify-content-md-center">
    <div class="col-lg-6">
        <div class="card card-default collapsed-card">
          <div class="card-header" style="background-color: #7B7A7A;"> <!-- #b818e2 -->

      <h3 >โรงพยาบาลสิรินธร จังหวัดขอนแก่น</h3>
      <p class="small">
        เว็บไซท์ :
        <a href="http://203.157.144.198/web" target="_blank" >www.sirinthorn.net</a>
      </p>
      <p>

        <a class="btn btn-outline btn-lg" href="#" >
          <span class="fa fa-book"></span>
          คู่มือการใช้งาน
        </a>

      </p>
      <ul class="jumbotron-links">
        <li><a href="https://www.facebook.com/rachzadej"><i class="fa fa-code"></i> coding by rachadej</a></li>
        <li><a href="http://203.157.144.198/app/"  ><i class="fa fa-flag"></i> งานเทคโนโลยีสารสนเทศ</a></li>
      </ul>
    </div>

    </div>
    </div>
  </div>

    </div>

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">



      <div class="row">
      <div class="col-12">
      <h5 class="mt-4 mb-2">Smart Health ID</h5>
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">รายละเอียด</h3>

                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">ข้อมูลทั่วไป</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">ที่อยู่</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">แพ้ยา</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">สิทธิ์การรักษา</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab">รูปภาพ</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <div class="tab-pane active" id="tab_1">
<p>
                  <span><button type="button" class="btn btn-info" id="getdata"  >อ่านข้อมูลจากบัตร</button> </span>

                  <span class="pull-right">

                    <form class="form-inline ml-4" id="form_cid">
                      <div class="input-group input-group-sm">
                        <input class="form-control" type="search" id="scid" placeholder="ค้นหาโดย CID"  aria-label="Search" value="0105579234511">
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </form>

                  </span>
    </p>
                    <div class="table-responsive" id="tb_data">

                    <table class="table table-striped table-bordered table-sm">

                        <tr>
                            <td rowspan="4" class="text-center" id="image1">
                                <img src="./dist/img/blank.png" id="image2" class="img-fluid" width="128" >
                            </td>

                            <th class="bg-warning">รายการ</th>
                            <th class="bg-warning">รายละเอียด</th>
                            <th class="bg-warning">รายการ</th>
                            <th class="bg-warning">รายละเอียด</th>
                        </tr>
                       <tr>
                        <td>เลขที่บัตรประชาชน</td>
                        <td id="cid"></td>
                        <td>เลขที่ประจำบ้าน</td>
                        <td id="hid"></td>
                       </tr>
                       <tr>
                        <td>คำนำหน้า</td>
                        <td id="tname"></td>
                        <td>ชื่อ-สกุล</td>
                        <td id="fullname"></td>
                       </tr>
                       <tr>
                        <td>วันเกิด</td>
                        <td id="bdate"></td>
                        <td>เพศ</td>
                        <td id="sex"></td>
                       </tr>
                    </table>

                    </div>

                    <div class="table-responsive">
                    <table class="table table-sm table-striped table-bordered ">
                    <thead>
                      <tr class="bg-dark">
                        <th>รายการ</th><th>รายละเอียด</th><th>รายการ</th><th>รายละเอียด</th><th>รายการ</th><th>รายละเอียด</th>
                      </tr>
                    </thead>
                    <tr>
                      <td width="200">BIRTH_MOI</td><td id="birth_moi"></td><td width="200">สถานะสมรส</td><td id="mstatus"></td><td width="200">อาชีพ(รหัสเก่า)</td><td id="occupation_old"></td>
                    </tr>
                    <tr>
                      <td>อาชีพ(รหัสใหม่)</td><td id="occupation_new"></td><td>เชื้อชาติ</td><td id="race"></td><td>สัญชาติ</td><td id="nation"></td>
                    </tr>
                    <tr>
                      <td>ศาสนา</td><td id="religion"></td><td>ระดับการศึกษา</td><td id="education"></td><td>รหัส CID บิดา</td><td id="father"></td>
                    </tr>
                    <tr>
                      <td>รหัส CID มารดา</td><td id="mother"></td><td>สถานะในครอบครัว</td><td id="fstatus"></td><td>รหัส CID คู่สมรส</td><td id="couple"></td>
                    </tr>
                    <tr>
                      <td>สถานะ/สาเหตุการจําหน่าย</td><td id="discharge"></td><td>วันที่จําหน่าย</td><td id="ddischarge"></td><td>DATE_DISCHARGE_MOI</td><td id="date_discharge_moi"></td>
                    </tr>
                    <tr>
                      <td>หมู่เลือด</td><td id="abogroup"></td><td>หมู่เลือด RH</td><td id="rhgroup"></td><td>รหัสความเป็นคนต่างด้าว</td><td id="labor"></td>
                    </tr>
                    <tr>
                      <td>VSTATUS</td><td id="vstatus"></td><td>วันที่ย้ายเข้ามาเขตพื้นที่</td><td id="movein"></td><td>เลขที่ passport</td><td id="passport"></td>
                    </tr>
                    <tr>
                      <td>สถานะบุคคล</td><td id="typearea"></td><td>วันเดือนปีที่ปรับปรุงข้อมูล</td><td id="d_update"></td><td></td><td ></td>
                    </tr>
                    </table>
                    </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                  <div id="tab-address-border">
                                                    <table class="table table-striped" style="margin-top: 0px;">
                                                        <tr>
                                                            <th width="13%">addresstype</th>
                                                            <td width="20%"><span id="addresstype"></span></td>
                                                            <th width="13%">houseId</th>
                                                            <td width="20%"><span id="houseId"></span></td>
                                                            <th width="14%">housetype</th>
                                                            <td width="20%"><span id="housetype"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>roomno</th>
                                                            <td><span id="roomno"></span></td>
                                                            <th>condo</th>
                                                            <td><span id="condo"></span></td>
                                                            <th>houseno</th>
                                                            <td><span id="houseno"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>soisub</th>
                                                            <td><span id="soisub"></span></td>
                                                            <th>soimain</th>
                                                            <td><span id="soimain"></span></td>
                                                            <th>road</th>
                                                            <td><span id="road"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>villaname</th>
                                                            <td><span id="villaname"></span></td>
                                                            <th>village</th>
                                                            <td><span id="village"></span></td>
                                                            <th>tambon</th>
                                                            <td><span id="tambon"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>ampur</th>
                                                            <td><span id="ampur"></span></td>
                                                            <th>changwat</th>
                                                            <td><span id="changwat"></span></td>
                                                            <th>telephone</th>
                                                            <td><span id="telephone"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>mobile</th>
                                                            <td><span id="mobile"></span></td>
                                                        </tr>
                                                    </table>
                                                </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">

                  <div class="table-responsive">
                                                        <table class="table table-bordered table-striped" style="margin-bottom: 10px;">
                                                            <thead>
                                                                <tr>
                                                                    <th width="10%" class="text-center">วันที่</th>
                                                                    <th width="10%" class="text-center">รหัสยา</th>
                                                                    <th width="15%" class="text-center">ชื่อยา</th>
                                                                    <th width="15%" class="text-center">ประเภทการวินิจฉัย</th>
                                                                    <th width="15%" class="text-center">ระดับความรุนแรง</th>
                                                                    <th width="15%" class="text-center">ลักษณะอาการแพ้</th>
                                                                    <th width="10%" class="text-center">ผู้ให้ประวัติ</th>
                                                                    <th width="10%" class="text-center">สถานพยาบาล</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="drugallergyTbody">

                                                            </tbody>
                                                        </table>
                                                    </div>


                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_4">
                            <div class="table-responsive">
                                                        <table class="table table-bordered table-sm">
                                                            <tr>
                                                                <th width="13%" class="bg-gray">เลขบัตรประชาชน</th>
                                                                <td width="20%" id="nhso_cid"></td>
                                                                <th width="13%" class="bg-gray">ชื่อ-นามสกุล</th>
                                                                <td width="20%" id="nhso_name"></td>
                                                                <th width="14%" class="bg-gray">สิทธิ หลัก.</th>
                                                                <td width="20%" id="nhso_right_main"></td>
                                                            </tr>
                                                            <tr>
                                                                <th class="bg-gray">สิทธิ รอง.</th>
                                                                <td id="nhso_right_sub"></td>
                                                                <th class="bg-gray">เลขที่หลักสิทธิหลัก</th>
                                                                <td id="nhso_main_code"></td>
                                                                <th class="bg-gray">รพ.หลัก</th>
                                                                <td id="nhso_main_hos"></td>
                                                            </tr>
                                                            <tr>
                                                                <th class="bg-gray">รพ.รอง</th>
                                                                <td id="nhso_sub_hos"></td>
                                                                <th class="bg-gray">วันเริ่มมีสิทธิ</th>
                                                                <td id="nhso_start"></td>
                                                                <th class="bg-gray">วันหมดอายุ</th>
                                                                <td id="nhso_end"></td>
                                                            </tr>

                                                        </table>
                          </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_5">


                  </div>
                  <!-- /.tab-pane -->

                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->


      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<script>


$('#getdata').click(function(){
  var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:8084/smartcard/data/",
      "method": "GET"
    }
  $.ajax(settings).done(function (response) {
      var jwt_token = localStorage.getItem("jwt_token");
      var obj = jQuery.parseJSON(response);
    // var obj = JSON.parse(response;)
      var cid = obj.cid;
      var sex = "";
      if (obj.gender=='1') {
        sex = "ชาย";
      } else {
        sex = "หญิง";
      }
      $('#cid').html(cid);
      $('#tname').html(obj.prename);
      $('#fullname').html(obj.fname+" "+obj.lname);
      $('#sex').html(sex);
      $('#bdate').html(datethai(obj.dob));
      $('#image2').attr("src","http://localhost:8084/smartcard/picture/");

      show_data(cid,jwt_token);

    });
});

$('#form_cid').submit(function(e){
  e.preventDefault();
  var cid = $('#scid').val();
  var jwt_token = localStorage.getItem("jwt_token");
  $('#image2').attr("src","./dist/img/blank.png");
  show_data(cid,jwt_token);

});

function show_data(cid,jwt_token){

      var settings2 = {
        "async": true,
        "crossDomain": true,
        "url": "https://smarthealth.service.moph.go.th/phps/api/person/v2/findby/cid/?cid="+cid,
        "method": "GET",
        "headers": {
          "jwt-token": jwt_token
          }
        }
        $.ajax(settings2).done(function (data) {
          //console.log(data);

          if (data.sex=='1') {
            sex = "ชาย";
          } else {
            sex = "หญิง";
          }
          $('#cid').html(cid);
          $('#tname').html(data.prename_moi);
          $('#fullname').html(data.name+" "+data.lname);
          $('#sex').html(sex);
          $('#bdate').html(datethai1(data.birth));

          $('#hid').html(data.hid);
          $('#birth_moi').html(datethai1(data.birth_moi));
          $('#mstatus').html(data.mstatus);
          $('#occupation_old').html(data.occupation_old);
          $('#occupation_new').html(data.occupation_new);
          $('#nation').html(data.nation);
          $('#religion').html(data.religion);
          $('#education').html(data.education);
          $('#father').html(data.father);
          $('#mother').html(data.mother);
          $('#fstatus').html(data.fstatus);
          $('#couple').html(data.couple);
          $('#discharge').html(data.discharge);
          $('#ddischarge').html(data.ddischarge);
          $('#abogroup').html(data.abogroup);
          $('#rhgroup').html(data.rhgroup);
          $('#vstatus').html(data.vstatus);
          $('#movein').html(datethai1(data.movein));
          $('#date_discharge_moi').html(data.date_discharge_moi);
          $('#labor').html(data.labor);
          $('#passport').html(data.passport);
          $('#typearea').html(data.typearea);
          $('#d_update').html(data.d_update);
        });

        var settings3 = {
          "async": true,
          "crossDomain": true,
          "url": "https://smarthealth.service.moph.go.th/phps/api/address/v1/find_by_cid?cid="+cid,
          "method": "GET",
          "headers": {
            "jwt-token": jwt_token
          }
        }

        $.ajax(settings3).done(function (addr) {
          //console.log(addr);
          $("#addresstype").html(addr.addresstype);
            $("#houseId").html(addr.houseId);
            $("#housetype").html(addr.housetype);
            $("#roomno").html(addr.roomno);
            $("#condo").html(addr.condo);
            $("#houseno").html(addr.houseno);
            $("#soisub").html(addr.soisub);
            $("#soimain").html(addr.soimain);
            $("#road").html(addr.road);
            $("#villaname").html(addr.villaname);
            $("#village").html(addr.village);
            $("#tambon").html(addr.tambon);
            $("#ampur").html(addr.ampur);
            $("#changwat").html(addr.changwat);
            $("#telephone").html(addr.telephone);
            $("#mobile").html(addr.mobile);
        });

        var settings4 = {
          "async": true,
          "crossDomain": true,
          "url": "https://smarthealth.service.moph.go.th/phps/api/drugallergy/v1/find_by_cid?cid="+cid,
          "method": "GET",
          "headers": {
            "jwt-token": jwt_token
          }
        }

        $.ajax(settings4).done(function (drugallergy) {
          //console.log(drugallergy);
          $('#drugallergyTbody').html("");
          if (Object.keys(drugallergy).length > 0) {
            var drugLine = drugallergy.data;
            $.each(drugLine, function (index, obj) {
                var table = '';
                table += '<tr>';
                table += '	<td>' + obj.daterecord + '</td>';
                table += '	<td>' + obj.drugcode + '</td>';
                table += '	<td>' + obj.drugname + '</td>';
                table += '	<td>' + obj.typedxcode + ' : ' + obj.typedx + '</td>';
                table += '	<td>' + obj.allerglevelcode + ' : ' + obj.allerglevel + '</td>';
                table += '	<td>' + obj.allergsymptomcode + ' : ' + obj.allergsymptom + '</td>';
                table += '	<td>' + obj.informat + '</td>';
                table += '	<td>' + obj.inforhosp + '</td>';
                table += '</tr>';

                $("#drugallergyTbody").append(table);

            });
            if (drugLine.length == 0) {
                $("#drugallergyTbody").html('<tr><td colspan="8" class="text-center">ไม่มีข้อมูล</td></tr>');
            }
         }
        });

        var settings5 = {
          "async": true,
          "crossDomain": true,
          "url": "https://smarthealth.service.moph.go.th/phps/api/nhsodata/v1/search_by_pid?cid="+cid,
          "method": "GET",
          "headers": {
            "jwt-token": jwt_token
          }
        }

        $.ajax(settings5).done(function (nhso) {
          //console.log(nhso);
          $("#nhso_cid").html(nhso.personId);
          $("#nhso_name").html(nhso.fname + " " + nhso.lname);
          $("#nhso_right_main").html(nhso.maininsclName);
          $("#nhso_right_sub").html(nhso.subinsclName);
          $("#nhso_main_code").html(nhso.maininscl);
          $("#nhso_main_hos").html(nhso.hmainOpName);
          $("#nhso_sub_hos").html(nhso.hsubName);
          $("#nhso_start").html(nhso.startdate);
          $("#nhso_end").html(nhso.expdate);
        });
        var settings6 = {
            "async": true,
            "crossDomain": true,
            "url": "https://smarthealth.service.moph.go.th/phps/api/00031/009/01",
            "method": "POST",
            "headers": {
              "Content-Type": "text/plain",
              "dataType": "application/json",
              "jwt-token": jwt_token
            },
            "data": cid
          }

          $.ajax(settings6).done(function (mou) {
            console.log(mou);
            //alert(mou.error);
            if (mou.error == 'false') {
                         
              var mou1 = mou.data.return;
              $("#nhso_cid").html(mou1.personId);
              $("#nhso_name").html(mou1.fname + " " + mou1.lname);
              $("#nhso_right_main").html(mou1.maininsclName);
              $("#nhso_right_sub").html(mou1.subinsclName);
              $("#nhso_main_code").html(mou1.maininscl);
              $("#nhso_main_hos").html(mou1.hmainName);
              $("#nhso_sub_hos").html(mou1.hsubName);
              $("#nhso_start").html(datethai(mou1.startdate));
              $("#nhso_end").html(mou1.expdate);
            }
          });

/*
          var settings7 = {
            "async": true,
            "crossDomain": true,
            "url": "https://smarthealth.service.moph.go.th/phps/api/00023/038/01",
            "method": "POST",
            "headers": {
              "Content-Type": "text/plain",
              "dataType": "application/json",
              "jwt-token": jwt_token
            },
            "data": cid
          }

          $.ajax(settings7).done(function (pict) {
            console.log(pict);
            
          });
*/


}



</script>
